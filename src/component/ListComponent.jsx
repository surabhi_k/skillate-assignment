import React from 'react';
import { Grid, Row, Col, FormGroup, ControlLabel, FormControl } from "react-bootstrap";
import { withRouter } from "react-router-dom";

class ListComponent extends React.Component{
  constructor(props) {
    super(props);
    this.state = {
      list : ["Label", "Label", "Label"],
      selectedItems: []
    };
    this.removeLabel = this.removeLabel.bind(this);
    this.addItems = this.addItems.bind(this);
    this.selectItems = this.selectItems.bind(this);
    this.removeAll = this.removeAll.bind(this);
  }

  render() {
    return(
      <div className="main-wrapper container">
        <Grid>
          <Row>
            <Col md={8} mdOffset={2} sm={12}>
              <div className="label-containter">
                <div className="section-header">
                  <span className="label">
                     {this.state.selectedItems.length > 0 ?
                      (<div className="enabled">
                        <i className="fa fa-minus" aria-hidden="true"></i>
                      </div>) :
                      (
                        <div className="disabled"></div>
                      )
                    }
                    Section Heading
                  </span>
                  <div  className="text">
                    <span className="status"> {this.state.selectedItems.length} Labels Selected </span>
                    <span className={"btn " + (this.state.selectedItems.length > 0 ? 'btn-active' : 'btn-disabled')} type="button" onClick={this.removeAll}>REMOVE</span>
                  </div>
                </div>

                <ul>
                  {
                    this.state.list.map((item, i) => {
                      return (
                        <li key={i}>
                          {(i === 0) ? (<div className="disabled"></div>) :
                            (this.state.selectedItems && this.state.selectedItems.indexOf(i) === -1 ?
                            (<div className="unchecked" onClick={() => this.selectItems(i)}></div>) :
                            (<div className="checked" onClick={() => this.selectItems(i)}>
                              <i className="fa fa-check" aria-hidden="true"></i>
                            </div>))}
                          <form>
                            <FormGroup>
                                <label>{`${item} ${i + 1}`}</label>
                                <FormControl
                                  type="text"
                                  value={this.state.item}
                                  placeholder=""
                                  onChange={this.handleChange}
                                />
                              </FormGroup>
                          </form>
                          {i !== 0 &&<div type="button" className="remove-btn" onClick={() => this.removeLabel(i)}>REMOVE</div>}
                        </li>
                      )
                    })
                  }
                </ul>

                <div  className="add-btn">
                  <hr />
                  <span type="button" onClick={this.addItems}>
                    <i className="fa fa-plus" aria-hidden="true"></i>
                    ADD LABEL
                  </span>
                </div>
              </div>
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }

  /* Private Methods */
  removeLabel(id) {
    let list = this.state.list;
    let found = list.indexOf(list[id]);
    if(found > -1) {
      list.splice(found, 1);
    }
    this.setState({
      list
    });
  }

  addItems() {
    let list = this.state.list;
    // Can be customised to push object also
    list.push("Label");
    this.setState({
      list
    });
  }

  selectItems(id) {
    let selectedItems = this.state.selectedItems;
    if(selectedItems.indexOf(id) !== -1) {
      selectedItems.splice(selectedItems.indexOf(id), 1);
    } else {
      selectedItems.push(id);
    }
    this.setState({
      selectedItems
    });
  }

  removeAll() {
    let selectedItems = this.state.selectedItems,
        list = this.state.list;
    selectedItems.map((item) => {
      list.splice(selectedItems.indexOf(item), 1);
    });
    this.setState({
      list,
      selectedItems: []
    });
  }
}


export default withRouter(ListComponent);
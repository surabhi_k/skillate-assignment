import React from 'react';
import styles from "../../assets/css/style.css";
import { Link } from 'react-router-dom';

import { Navbar, Nav, NavItem, Glyphicon } from 'react-bootstrap';

class Header extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <Navbar fluid>
        <div class="container">
          <Navbar.Header>
            <Navbar.Brand>
                <h5>
                  Category
                </h5>
                <h3>
                  Page Heading
                </h3>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Nav pullRight>
            <button>BUTTON</button>
          </Nav>
        </div>
      </Navbar>
    );
  }
}


export default Header;
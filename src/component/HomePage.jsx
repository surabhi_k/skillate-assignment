import React from 'react';
import { Grid, Row, Col,Button, Glyphicon } from "react-bootstrap";
import { withRouter } from "react-router-dom";
import Header from "../component/common/Header.jsx";
import ListComponent from "../component/ListComponent.jsx";

class HomePage extends React.Component{
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return(
      <React.Fragment>
        <Header/>
        <ListComponent />
      </React.Fragment>
    );
  }
}


export default withRouter(HomePage);
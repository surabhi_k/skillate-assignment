import React, { Component } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import ReactDOM from 'react-dom';
import HomePage from './src/component/HomePage.jsx';
import 'font-awesome/css/font-awesome.css';

class App extends Component{
    render(){
        return(
            <main>
                <Switch>
                    <Route exact path='/' component={HomePage}/>
                </Switch>
            </main>
        );
    }
}

ReactDOM.render(<BrowserRouter><App /></BrowserRouter> , document.querySelector('.main'));